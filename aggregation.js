/*
[SECTION] MongoDB Aggregation

Used to generate, manipulate, and perform operations to create filtered results that helps us to analyze dat.

using the aggregate method:


the $match
is used to pass the document that meet the specified conditions to the next stage / aggregation process.

syntax:

{$match: {field: value}}

the $group
is used to group elements together and field-value pairs the data from the grouped elements

syntax:

{$group: {_id: "value", fieldResult: "valueResult"}}


using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stock for all suppliers found.

*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{$sum:"$stock"}
			}}
	])


/*
Field projection with aggregation

the "$project" can be used when aggregating data to include/exclude fields from the returned result

syntax:

($project:{field:1/10})
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{$sum:"$stock"}
			}},
		{$project: {_id: 0}}
	]);


/*
Sorting Aggregated Results

the "$sort" can be used to change the order of the new aggregated result

syntax:

{$sort: {field:1/-1}}
1 -> lowest to highest
2 -> highest to lowest
*/


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{$sum:"$stock"}
			}},
		{$sort: {total: 1}}
	]);


/*
Aggregating results based on array fields

the "$unwind" deconstructs an array field from a collection / field with an array value to output a result

syntax:

{$unwind: field}
*/


db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds:{$sum: 1}}}
	]);




/*

[SECTION] Other Aggregate stages

// $count all yellow fruits


*/


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$count: "Yellow Fruits"},
	]);



//$avg gets the average value of stock


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group:{_id:"$color", yellow_fruits_stock:{$avg:"$stock"}}}
	]);



//$min & $max


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group:{_id:"$color", yellow_fruits_stock:{$min:"$stock"}}}
	]);


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group:{_id:"$color", yellow_fruits_stock:{$max:"$stock"}}}
	]);

